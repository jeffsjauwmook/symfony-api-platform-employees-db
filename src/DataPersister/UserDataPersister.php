<?php

namespace App\DataPersister;

use App\Entity\User;
use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

final class UserDataPersister implements ContextAwareDataPersisterInterface
{
    protected $passwordEncoder;
    protected $decorated;

    public function __construct(DataPersisterInterface $decorated, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->decorated = $decorated;
    }



    public function supports($data, array $context = []): bool
    {
        return $data instanceof User;
    }

    public function persist($data, array $context = [])
    {
        /** @var User $user */
        $user = $data;
        $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPassword()));
        $this->decorated->persist($user, $context);
        return $user;
    }

    public function remove($data, array $context = [])
    {
    }
}
