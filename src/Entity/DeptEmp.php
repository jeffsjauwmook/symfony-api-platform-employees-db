<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource
 * @ORM\Table(name="dept_emp", indexes={@ORM\Index(name="dept_no", columns={"dept_no"}), @ORM\Index(name="emp_id", columns={"emp_id"})})
 * @ORM\Entity
 */
class DeptEmp
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=false)
     * @Assert\Type("\DateTimeInterface")
     */
    public $fromDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=false)
     * @Assert\Type("\DateTimeInterface")
     */
    public $toDate;

    /**
     * @var \Employee
     *
     * @ORM\ManyToOne(targetEntity="Employee")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="emp_id", referencedColumnName="id")
     * })
     */
    public $emp;

    /**
     * @var \Department
     *
     * @ORM\ManyToOne(targetEntity="Department")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dept_no", referencedColumnName="id")
     * })
     */
    public $deptNo;
}
