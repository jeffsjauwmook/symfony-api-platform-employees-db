<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource
 * @ORM\Table(name="salaries", indexes={@ORM\Index(name="emp_id", columns={"emp_id"})})
 * @ORM\Entity
 */
class Salary
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     * @Assert\NotBlank
     * @Assert\Type("integer")
     */
    public $salary;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="from_date", type="date", nullable=false)
     * @Assert\Type("\DateTimeInterface")
     */
    public $fromDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="to_date", type="date", nullable=false)
     * @Assert\Type("\DateTimeInterface")
     */
    public $toDate;

    /**
     * @var \Employee
     *
     * @ORM\ManyToOne(targetEntity="Employee")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="emp_id", referencedColumnName="id")
     * })
     */
    public $emp;

    /**
     * Get the value of id
     *
     * @return  int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of salary
     *
     * @return  int
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * Set the value of salary
     *
     * @param  int  $salary
     *
     * @return  self
     */
    public function setSalary(int $salary)
    {
        $this->salary = $salary;

        return $this;
    }

    /**
     * Get the value of fromDate
     *
     * @return  \DateTime
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Set the value of fromDate
     *
     * @param  \DateTime  $fromDate
     *
     * @return  self
     */
    public function setFromDate(\DateTime $fromDate)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * Get the value of toDate
     *
     * @return  \DateTime
     */
    public function getToDate()
    {
        return $this->toDate;
    }

    /**
     * Set the value of toDate
     *
     * @param  \DateTime  $toDate
     *
     * @return  self
     */
    public function setToDate(\DateTime $toDate)
    {
        $this->toDate = $toDate;

        return $this;
    }

    /**
     * Get the value of emp
     *
     * @return  \Employee
     */
    public function getEmp()
    {
        return $this->emp;
    }

    /**
     * Set the value of emp
     *
     * @param  Employee  $emp
     *
     * @return  self
     */
    public function setEmp(Employee $emp)
    {
        $this->emp = $emp;

        return $this;
    }
}
