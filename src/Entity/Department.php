<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource
 * @ORM\Table(name="department", uniqueConstraints={@ORM\UniqueConstraint(name="dept_name", columns={"dept_name"})})
 * @ORM\Entity
 */
class Department
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=40, nullable=false)
     * @Assert\NotBlank
     */
    public $deptName;

    /**
     * Get the value of id
     *
     * @return  int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Get the value of deptName
     *
     * @return  string
     */
    public function getDeptName()
    {
        return $this->deptName;
    }

    /**
     * Set the value of deptName
     *
     * @param  string  $deptName
     *
     * @return  self
     */
    public function setDeptName(string $deptName)
    {
        $this->deptName = $deptName;

        return $this;
    }
}
