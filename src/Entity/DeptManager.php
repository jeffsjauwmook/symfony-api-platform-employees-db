<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource
 * @ORM\Table(name="dept_managers", indexes={@ORM\Index(name="dept_no", columns={"dept_no"}), @ORM\Index(name="emp_id", columns={"emp_id"})})
 * @ORM\Entity
 */
class DeptManager
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=false)
     * @Assert\Type("\DateTimeInterface")
     */
    public $fromDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=false)
     * @Assert\Type("\DateTimeInterface")
     */
    public $toDate;

    /**
     * @var \Employee
     *
     * @ORM\ManyToOne(targetEntity="Employee")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="emp_id", referencedColumnName="id")
     * })
     */
    public $emp;

    /**
     * @var \Department
     *
     * @ORM\ManyToOne(targetEntity="Department")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dept_no", referencedColumnName="id")
     * })
     */
    public $deptNo;

    /** 
     * @var User
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user", referencedColumnName="id")
     * })
     */
    public $user;

    /**
     * Get the value of id
     *
     * @return  int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of fromDate
     *
     * @return  \DateTime
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Set the value of fromDate
     *
     * @param  \DateTime  $fromDate
     *
     * @return  self
     */
    public function setFromDate(\DateTime $fromDate)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * Get the value of emp
     *
     * @return  Employee
     */
    public function getEmp()
    {
        return $this->emp;
    }

    /**
     * Set the value of emp
     *
     * @param  \Employee  $emp
     *
     * @return  self
     */
    public function setEmp(Employee $emp)
    {
        $this->emp = $emp;

        return $this;
    }

    /**
     * Get the value of deptNo
     *
     * @return  \Department
     */
    public function getDeptNo()
    {
        return $this->deptNo;
    }

    /**
     * Set the value of deptNo
     *
     * @param  Department  $deptNo
     *
     * @return  self
     */
    public function setDeptNo(Department $deptNo)
    {
        $this->deptNo = $deptNo;

        return $this;
    }

    /**
     * Get })
     *
     * @return  User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set })
     *
     * @param  User  $user  })
     *
     * @return  self
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }
}
