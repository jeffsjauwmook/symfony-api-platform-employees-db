<?php

namespace App\Entity;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource
 * @ORM\Table(name="employees")
 * @ORM\Entity
 */
class Employee
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=false)
     * @Assert\Type("\DateTimeInterface")
     */
    public $birthDate;

    /**
     * @var string
     *
     * @ORM\Column(length=14, nullable=false)
     * @Assert\NotBlank
     */
    public $firstName;

    /**
     * @var string
     *
     * @ORM\Column(length=16, nullable=false)
     * @Assert\NotBlank
     */
    public $lastName;

    /**
     * @var string
     *
     * @ORM\Column(length=0, nullable=false)
     * @Assert\NotBlank
     */
    public $gender;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=false)
     * @Assert\Type("\DateTimeInterface")
     */
    public $hireDate;

    /** 
     * @var User
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user", referencedColumnName="id")
     * })
     */
    public $user;

    /**
     * Get the value of id
     *
     * @return  int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of birthDate
     *
     * @return  \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set the value of birthDate
     *
     * @param  \DateTime  $birthDate
     *
     * @return  self
     */
    public function setBirthDate(\DateTime $birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get the value of firstName
     *
     * @return  string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set the value of firstName
     *
     * @param  string  $firstName
     *
     * @return  self
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get the value of lastName
     *
     * @return  string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set the value of lastName
     *
     * @param  string  $lastName
     *
     * @return  self
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get the value of gender
     *
     * @return  string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set the value of gender
     *
     * @param  string  $gender
     *
     * @return  self
     */
    public function setGender(string $gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get the value of hireDate
     *
     * @return  \DateTime
     */
    public function getHireDate()
    {
        return $this->hireDate;
    }

    /**
     * Set the value of hireDate
     *
     * @param  \DateTime  $hireDate
     *
     * @return  self
     */
    public function setHireDate(\DateTime $hireDate)
    {
        $this->hireDate = $hireDate;

        return $this;
    }

    /**
     *
     * @return  User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     *
     * @param  User  $user  })
     *
     * @return  self
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }
}
