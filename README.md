## This project is a Symfony API platform boilerplate  

In this example I have converted the [Employees database](https://dev.mysql.com/doc/employee/en/) to a Symfony Compatible format.


## To run this code:
Install docker and docker-compose
Then run : 
## `Docker-compose up --build`
After the docker containers have been build:

## `docker exec -i -u  www-data  employees-php-fpm  composer install`
## `docker exec -i -u  www-data  employees-php-fpm  bin/console d:d:c`
## `docker exec -i -u  www-data  employees-php-fpm  bin/console d:m:m`
## `docker exec -i -u  www-data  employees-php-fpm  bin/console d:f:l`

You can then access the API docs via 
## `http://172.30.238.10/docs`


You can get valid token bij loggin with :
## `email:test@test.dev	password:mySecretPass123`

on the url 
## `http://172.30.238.10/authentication_token`

