<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200629075851 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE department (id INT AUTO_INCREMENT NOT NULL, dept_name VARCHAR(40) NOT NULL, UNIQUE INDEX dept_name (dept_name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dept_emp (id INT AUTO_INCREMENT NOT NULL, emp_id INT DEFAULT NULL, dept_no INT DEFAULT NULL, from_date DATE NOT NULL, to_date DATE NOT NULL, INDEX dept_no (dept_no), INDEX emp_id (emp_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dept_managers (id INT AUTO_INCREMENT NOT NULL, emp_id INT DEFAULT NULL, dept_no INT DEFAULT NULL, user INT DEFAULT NULL, from_date DATE NOT NULL, to_date DATE NOT NULL, UNIQUE INDEX UNIQ_11CF2B8B8D93D649 (user), INDEX dept_no (dept_no), INDEX emp_id (emp_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employees (id INT AUTO_INCREMENT NOT NULL, user INT DEFAULT NULL, birth_date DATE NOT NULL, first_name VARCHAR(14) NOT NULL, last_name VARCHAR(16) NOT NULL, gender VARCHAR(255) NOT NULL, hire_date DATE NOT NULL, UNIQUE INDEX UNIQ_BA82C3008D93D649 (user), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE salaries (id INT AUTO_INCREMENT NOT NULL, emp_id INT DEFAULT NULL, salary INT NOT NULL, from_date DATE NOT NULL, to_date DATE NOT NULL, INDEX emp_id (emp_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE titles (id INT AUTO_INCREMENT NOT NULL, emp_id INT DEFAULT NULL, title VARCHAR(50) NOT NULL, from_date DATE NOT NULL, to_date DATE DEFAULT NULL, INDEX emp_id (emp_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, api_token VARCHAR(255) DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_1483A5E97BA2F5EB (api_token), UNIQUE INDEX UNIQ_1483A5E9E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE dept_emp ADD CONSTRAINT FK_B2592B4D7A663008 FOREIGN KEY (emp_id) REFERENCES employees (id)');
        $this->addSql('ALTER TABLE dept_emp ADD CONSTRAINT FK_B2592B4DE6B0AD08 FOREIGN KEY (dept_no) REFERENCES department (id)');
        $this->addSql('ALTER TABLE dept_managers ADD CONSTRAINT FK_11CF2B8B7A663008 FOREIGN KEY (emp_id) REFERENCES employees (id)');
        $this->addSql('ALTER TABLE dept_managers ADD CONSTRAINT FK_11CF2B8BE6B0AD08 FOREIGN KEY (dept_no) REFERENCES department (id)');
        $this->addSql('ALTER TABLE dept_managers ADD CONSTRAINT FK_11CF2B8B8D93D649 FOREIGN KEY (user) REFERENCES users (id)');
        $this->addSql('ALTER TABLE employees ADD CONSTRAINT FK_BA82C3008D93D649 FOREIGN KEY (user) REFERENCES users (id)');
        $this->addSql('ALTER TABLE salaries ADD CONSTRAINT FK_E6EEB84B7A663008 FOREIGN KEY (emp_id) REFERENCES employees (id)');
        $this->addSql('ALTER TABLE titles ADD CONSTRAINT FK_C14541A37A663008 FOREIGN KEY (emp_id) REFERENCES employees (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE dept_emp DROP FOREIGN KEY FK_B2592B4DE6B0AD08');
        $this->addSql('ALTER TABLE dept_managers DROP FOREIGN KEY FK_11CF2B8BE6B0AD08');
        $this->addSql('ALTER TABLE dept_emp DROP FOREIGN KEY FK_B2592B4D7A663008');
        $this->addSql('ALTER TABLE dept_managers DROP FOREIGN KEY FK_11CF2B8B7A663008');
        $this->addSql('ALTER TABLE salaries DROP FOREIGN KEY FK_E6EEB84B7A663008');
        $this->addSql('ALTER TABLE titles DROP FOREIGN KEY FK_C14541A37A663008');
        $this->addSql('ALTER TABLE dept_managers DROP FOREIGN KEY FK_11CF2B8B8D93D649');
        $this->addSql('ALTER TABLE employees DROP FOREIGN KEY FK_BA82C3008D93D649');
        $this->addSql('DROP TABLE department');
        $this->addSql('DROP TABLE dept_emp');
        $this->addSql('DROP TABLE dept_managers');
        $this->addSql('DROP TABLE employees');
        $this->addSql('DROP TABLE salaries');
        $this->addSql('DROP TABLE titles');
        $this->addSql('DROP TABLE users');
    }
}
